
import { pipe, partialRight } from 'ramda';
import { Map, Set } from 'immutable';

import { Entities, nextId, addEntities, updateEntity, updateEntities, aiMovingEntities, physicalEntities } from './entities';
import { createFieldPlayer } from './fieldPlayer';
import { Vec2 } from './math';
import { goToPosition, updateEntityBehavior } from './ai';
import { AIMoving, Entity, EntityId, TraitId } from './entity';
import { updateEntitiesPhysics, Time, Sec } from './physics';

export interface Game {
    entities: Entities
};

let entitiesMap: Entities = {
    lastId: 0 as EntityId,
    byId: Map<EntityId, Entity>(),
    [TraitId.PHYSICAL]: Set<EntityId>(),
    [TraitId.DISPLAYABLE]: Set<EntityId>(),
    [TraitId.AI_MOVING]: Set<EntityId>(),
};

entitiesMap = nextId(entitiesMap);
const fieldPlayer = createFieldPlayer(entitiesMap.lastId, {x: 50, y: 50});

entitiesMap = nextId(entitiesMap);
const fieldPlayer2 = createFieldPlayer(entitiesMap.lastId, {x: 100, y: 100});

export const startGame = (): Game => ({
    entities: addEntities(entitiesMap, [fieldPlayer, fieldPlayer2]),
});

const updateEntitiesBehavior = (game: Game, duration: Time<Sec>): Game => ({
    ...game,
    entities: updateEntities(
        game.entities,
        aiMovingEntities(game.entities).map((entity: Entity<AIMoving>): Entity<AIMoving> => updateEntityBehavior(entity, duration))
    ),
});

const updatePhysics = (game: Game, duration: Time<Sec>): Game => ({
    ...game,
    entities: updateEntities(game.entities, updateEntitiesPhysics(physicalEntities(game.entities), duration)),
});

export const update = (game: Game, duration: Time<Sec>): Game => pipe(partialRight(updateEntitiesBehavior, [duration]), partialRight(updatePhysics, [duration]))(game);

export const commandFieldPlayerToGoToPosition = (position: Vec2, fieldPlayerId: EntityId, game: Game): Game => {
    const fieldPlayer = game.entities.byId.get(fieldPlayerId);
    return {
        ...game,
        entities: updateEntity(game.entities, {...fieldPlayer, updateMovement: goToPosition(position)})
    };
};
