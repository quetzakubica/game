
import { interval } from 'rxjs';
import { timeInterval, pluck, map } from 'rxjs/operators';
import { Observable, TimeInterval } from 'rxjs';

import { Time, Sec, MSec, Frequency } from './physics';

const freqToDuration = (frequency: Frequency<Sec>): Time<Sec> => (1.0 / frequency) as Time<Sec>;
const msToSeconds = (value: Time<MSec>): Time<Sec> => (value / 1000.0) as Time<Sec>;
const secondsToMs = (value: Time<Sec>): Time<MSec> => (value * 1000.0) as Time<MSec>;

export const createTicker = (ticksPerSecond: Frequency<Sec>): Observable<Time<Sec>> => (
    interval(secondsToMs(freqToDuration(ticksPerSecond)))
        .pipe(timeInterval())
        .pipe(pluck<TimeInterval<number>, Time<MSec>>('interval'))
        .pipe(map(msToSeconds))
);