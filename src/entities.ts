
import { Set, Map } from 'immutable';

import { Entity, EntityId, Trait, TraitId, Displayable, Physical, AIMoving } from './entity';

type EntityIdMap<TRAIT = {}> = Map<EntityId, Entity<TRAIT>>;

export interface Entities {
    lastId: EntityId,
    byId: EntityIdMap,
    [TraitId.PHYSICAL]: Set<EntityId>,
    [TraitId.DISPLAYABLE]: Set<EntityId>,
    [TraitId.AI_MOVING]: Set<EntityId>
};

export const mapEntitiesToIds = <TRAIT = {}>(entities: Entity<TRAIT>[]): EntityIdMap<TRAIT> => entities.reduce(
    (byIdMap: EntityIdMap<TRAIT>, entity: Entity<TRAIT>): EntityIdMap<TRAIT> => byIdMap.set(entity.id, entity),
    Map()
);

export const nextId = (entities: Entities): Entities => ({
    ...entities,
    lastId: (entities.lastId + 1) as EntityId,
});

export const addEntities = (entities: Entities, entitiesToAdd: Array<Entity>): Entities => (
    entitiesToAdd.reduce((entities, entity) => addEntity(entities, entity), entities)
);

const addEntity = (entities: Entities, entity: Entity): Entities => (
    entity.traits.reduce((entities?: Entities, traitId?: TraitId): Entities => ({
        ...entities!,
        byId: entities!.byId.set(entity.id, entity),
        [traitId!]: entities![traitId!].add(entity.id),
    }), entities)
);

export const updateEntity = (entities: Entities, updatedEntity: Entity): Entities => ({
    ...entities,
    byId: entities.byId.set(updatedEntity.id, updatedEntity),
});

export const updateEntities = (entities: Entities, entitiesToUpdate: Entity[]): Entities => entitiesToUpdate.reduce(
    (entities: Entities, entity: Entity): Entities => updateEntity(entities, entity),
    entities
);

const entitiesOfTrait = <T extends Trait>(entities: Entities, traitId: TraitId): Entity<T>[] => (
    entities[traitId].map((entityId?: EntityId): Entity<T> => entities.byId.get(entityId!) as Entity<T>).toArray()
);

export const displayableEntities = (entities: Entities): Entity<Displayable>[] => entitiesOfTrait(entities, TraitId.DISPLAYABLE);
export const physicalEntities = (entities: Entities): Entity<Physical>[] => entitiesOfTrait(entities, TraitId.PHYSICAL);
export const aiMovingEntities = (entities: Entities): Entity<AIMoving>[] => entitiesOfTrait(entities, TraitId.AI_MOVING);