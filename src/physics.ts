
import { Brand } from './brand';
import { slice, concat, sort, head } from 'ramda';
import { stop } from './ai';
import { addVectors, scaleVector, vectorLength, subtractVectors } from './math';
import { Entity, Physical } from './entity';
import { mapEntitiesToIds } from './entities';

export type Unit<UNIT, OF> = number & { __unit: UNIT, __of: OF };
export type Meters = Unit<'meters', 'distance'>;
export type Sec = Unit<'seconds', 'time'>;
export type MSec = Unit<'miliseconds', 'time'>;
type TimeUnit = Sec | MSec;
export type Frequency<UNIT extends TimeUnit> = Brand<number, 'frequency'> & UNIT;
export type Time<UNIT extends TimeUnit> = Brand<number, 'time'> & UNIT;
type DistanceUnit = Meters;
export type Distance<UNIT extends DistanceUnit> = Brand<number, 'distance'> & UNIT;
export type Speed<DISTANCE_UNIT extends DistanceUnit, TIME_UNIT extends TimeUnit>  = Brand<number, 'speed'> & DISTANCE_UNIT & TIME_UNIT;

interface Collision {
    entity1: Entity<Physical>,
    entity2: Entity<Physical>,
    timeToImpact: Time<Sec>,
}

const movePhysical = (entity: Entity<Physical>, duration: Time<Sec>): Entity<Physical> => ({
    ...entity,
    position: addVectors(entity.position, scaleVector(entity.velocity, duration)),
});

export const updateEntitiesPhysics = (entities: Entity<Physical>[], duration: Time<Sec>): Entity<Physical>[] => {
    const collisions = findCollisions(entities, duration);
    
    if (collisions.length !== 0) {
        console.log(collisions);
        const collision = earliestCollision(collisions);
        const movedEntities = entities.map((entity: Entity<Physical>): Entity<Physical> => movePhysical(entity, collision.timeToImpact));
        const entitiesMap = mapEntitiesToIds<Physical>(movedEntities);
        const resolvedEntities = resolveCollision(collision);
        const updatedEntities = resolvedEntities.reduce((byIdMap, entity) => byIdMap.set(entity.id, entity), entitiesMap).valueSeq().toArray();
        return updateEntitiesPhysics(updatedEntities, (duration - collision.timeToImpact) as Time<Sec>);
    } else {
        return entities.map((entity: Entity<Physical>): Entity<Physical> => movePhysical(entity, duration));
    }
};

const findCollisions = (entities: Entity<Physical>[], duration: Time<Sec>): Collision[] => entities.reduce(
    (collisions: Collision[], entity: Entity<Physical>, entityIndex: number): Collision[] => concat(collisions, slice(entityIndex + 1, Infinity, entities).reduce(
        (entityCollisions: Collision[], entityToCheckAgainst: Entity<Physical>): Collision[] => {
            const collision = collisionBetween(entity, entityToCheckAgainst, duration);
            return collision !== null ? concat(entityCollisions, [collision]) : entityCollisions;
        },
        []
    )),
    []
);

const collisionBetween = (entity1: Entity<Physical>, entity2: Entity<Physical>, duration: Time<Sec>): Collision | null => {
    const sumOfDistanceEntitiesCanMove = vectorLength(scaleVector(entity1.velocity, duration)) + vectorLength(scaleVector(entity2.velocity, duration));
    const distanceBetweenEntities = vectorLength(subtractVectors(entity2.position, entity1.position)) - entity1.shape.radius - entity2.shape.radius;
    return distanceBetweenEntities >= sumOfDistanceEntitiesCanMove ? null : {
        entity1,
        entity2,
        timeToImpact: 0.0 as Time<Sec>,
    };
};

const earliestCollision = (collisions: Collision[]): Collision => head(
    sort((a: Collision, b: Collision): number => a.timeToImpact - b.timeToImpact, collisions)
)!;

const resolveCollision = (collision: Collision): [Entity<Physical>, Entity<Physical>] => {
    return [
        {...collision.entity1, velocity: {x: 0, y: 0}, updateMovement: stop},
        {...collision.entity2, velocity: {x: 0, y: 0}, updateMovement: stop}
    ];
};