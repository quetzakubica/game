
import { Set } from  'immutable';

import { UpdateMovement } from './ai';
import { Meters, Sec, Speed, Distance } from './physics';
import { Vec2 } from './math';
import { Brand } from './brand';

export type EntityId = Brand<number, 'entityid'>;

export interface Displayable {
    position: Vec2,
};

export interface Physical {
    position: Vec2,
    velocity: Vec2,
    shape: {
        radius: Distance<Meters>,
    }
};
// todo: zmienic typ speed tak zeby mial jednostke predkosci
export type AIMoving = Physical & {
    speed: Speed<Meters, Sec>,
    updateMovement: UpdateMovement
};

export const enum TraitId {
    PHYSICAL = 'physical',
    DISPLAYABLE = 'displayable',
    AI_MOVING = 'aimoving',
};

export type Trait = Displayable | Physical | AIMoving;
export type Entity<T = {}> = {id: EntityId, traits: Set<TraitId>} & Trait & T;
