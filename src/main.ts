
import { Observable, merge } from 'rxjs';
import { scan, withLatestFrom, map } from 'rxjs/operators';

import { EntityId } from './entity';
import { startGame, Game, update, commandFieldPlayerToGoToPosition } from './game'
import { createView } from './view';
import { createTicker } from './ticker';
import { Frequency, Time, Sec } from './physics';
import { Vec2 } from './math';

type Command = (game: Game) => Game;

export const run = function() {
    console.log('Game has started.');

    const initialGameState = startGame();
    console.log(initialGameState)
    const view = createView(initialGameState);

    const TICKS_PER_SECOND = 30.0 as Frequency<Sec>;
    const ticks$ = createTicker(TICKS_PER_SECOND);
    
    const commands$: Observable<Command> = merge(
        ticks$.pipe(map((delta: Time<Sec>) => (game: Game): Game => update(game, delta))),
        view.stageLeftClickPosition$.pipe(map((position: Vec2) => (game: Game): Game => commandFieldPlayerToGoToPosition(position, 1 as EntityId, game)))
    );
    const game$ = commands$.pipe(scan((game: Game, command: Command): Game => command(game), initialGameState));

    ticks$.pipe(withLatestFrom(game$)).subscribe(([_, game]) => {
        view.update(game);
    });
};

run();
