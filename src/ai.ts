import { Entity, AIMoving } from './entity'
import { Vec2, subtractVectors, normalizeVector, scaleVector, vectorSqrLength, areVectorsEqual } from './math';

export type UpdateMovement = (entity: Entity<AIMoving>, updateDuration: number) => Entity<AIMoving>;

export const stop: UpdateMovement = (entity: Entity<AIMoving>, _: number): Entity<AIMoving> => ({
    ...entity,
    velocity: {x: 0, y: 0}
});

export const goToPosition = (position: Vec2): UpdateMovement => (entity: Entity<AIMoving>, updateDuration: number): Entity<AIMoving> => {
    const vecToPosition = subtractVectors(position, entity.position);
    const newVelocity = scaleVector(normalizeVector(vecToPosition), entity.speed);
    const isAlmostAtPosition = vectorSqrLength(vecToPosition) < vectorSqrLength(scaleVector(newVelocity, updateDuration));
    const isAtPosition = areVectorsEqual(entity.position, position);
    return {
        ...entity,
        velocity: isAlmostAtPosition ? vecToPosition : newVelocity,
        updateMovement: isAtPosition ? stop : entity.updateMovement
    };
};

export const updateEntityBehavior = (entity: Entity<AIMoving>, duration: number): Entity<AIMoving> => entity.updateMovement(entity, duration);