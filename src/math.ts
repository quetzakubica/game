
export interface Vec2 {
    x: number,
    y: number,
};

export const addVectors = (v1: Vec2, v2: Vec2): Vec2 => ({x: v1.x + v2.x, y: v1.y + v2.y});

export const subtractVectors = (v1: Vec2, v2: Vec2): Vec2 => ({x: v1.x - v2.x, y: v1.y - v2.y});

export const vectorLength = (v: Vec2): number => Math.sqrt(v.x ** 2 + v.y ** 2);

export const vectorSqrLength = (v: Vec2): number => v.x ** 2 + v.y ** 2;

export const normalizeVector = (v: Vec2): Vec2 => vectorSqrLength(v) === 0.0 ? v : {x: v.x / vectorLength(v), y: v.y / vectorLength(v)};

export const scaleVector = (v: Vec2, factor: number): Vec2 => ({x: v.x * factor, y: v.y * factor});

export const areVectorsEqual = (v1: Vec2, v2: Vec2): boolean => v1.x === v2.x && v1.y === v2.y;