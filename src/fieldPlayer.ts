
import { Set } from 'immutable';

import { Vec2 } from './math';
import { stop } from './ai';
import { Entity, EntityId, Displayable, Physical, AIMoving, TraitId } from './entity';
import { Meters, Sec, Speed, Distance } from './physics';

export type FieldPlayer = Entity<Displayable & Physical & AIMoving>;

export const createFieldPlayer = (id: EntityId, position: Vec2): FieldPlayer => ({
    id,
    traits: Set<TraitId>([TraitId.PHYSICAL, TraitId.DISPLAYABLE, TraitId.AI_MOVING]),
    position,
    velocity: {x: 0, y: 0},
    shape: {
        radius: 32.0 as Distance<Meters>,
    },
    speed: 50.0 as Speed<Meters, Sec>,
    updateMovement: stop,
});