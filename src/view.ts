
import { Application, Graphics, Point } from 'pixi.js';
import { fromEvent } from 'rxjs';
import { Map } from 'immutable';
import { map } from 'rxjs/operators';

import { Vec2 } from './math';
import { Game } from './game';
import { displayableEntities } from './entities';
import { Entity, Displayable, EntityId } from './entity';

type EntitiesViews = Map<EntityId, Graphics>;

export const createView = (game: Game) => {
    const app = new Application({width: 500, height: 500});
    document.body.appendChild(app.view);

    // change projection to cartesian
    app.stage.x = 0;
    app.stage.y = app.screen.height;
    app.stage.scale = new Point(1, -1);

    const entitiesViews = displayableEntities(game.entities).reduce(
        (entitiesViews: EntitiesViews, entity: Entity<Displayable>): Map<EntityId, Graphics> => entitiesViews.set(entity.id, createEntityView(entity)),
        Map<EntityId, Graphics>()
    ); 
    entitiesViews.valueSeq().forEach((entityView?: Graphics) => app.stage.addChild(entityView!));

    const canvasLeftClick$ = fromEvent<MouseEvent>(app.view, 'click');
    const canvasLeftClickPosition$ = canvasLeftClick$.pipe(map((ev: MouseEvent): Vec2 => ({x: ev.offsetX, y: ev.offsetY})));
    const stageLeftClickPosition$ = canvasLeftClickPosition$.pipe(map((pos:  Vec2): Vec2 => ({x: pos.x, y: app.screen.height - pos.y})));
    stageLeftClickPosition$.subscribe((p) => console.log(p));

    return {
        stageLeftClickPosition$,
        update: function(game: Game): void {
            let entity: Entity<Displayable>;
            entitiesViews.forEach((entityView?, entityId?) => {
                entity = game.entities.byId.get(entityId!);
                entityView!.position = new Point(entity.position.x, entity.position.y);
            })
            
        }
    };
};

const createEntityView = (entity: Entity<Displayable>): Graphics => {
    const entityView = new Graphics();
    entityView.beginFill(0x9966FF);
    entityView.drawCircle(0, 0, 32.0);
    entityView.endFill();
    entityView.position = new Point(entity.position.x, entity.position.y);
    return entityView;
};